<?php
// $Id: token_realname.inc,v 1.1.2.11 2010/10/27 07:29:16 davereid Exp $ */
/**
* @file
*   Token module support for the pdf_watermark module.
*/


/**
 * Implements hook_token_list().
 */
 function _pdf_watermark_token_list($type = 'all') {
   $tokens = array();
   	if ($type == 'pdfw' || $type == 'all') {
   		if (module_exists('profile')){
   		$tokens['pdfw']['pdf_watermark-fullname'] = t('The first and last name of user, gathered from Profile');
   		}
   		$tokens['pdfw']['pdf_watermark-user'] = t("The user's login name");
     	$tokens['pdfw']['pdf_watermark-email'] = t('The users email address.');
     	$tokens['pdfw']['pdf_watermark-small'] = t('The current date (small format).');
     	$tokens['pdfw']['pdf_watermark-medium'] = t('The current date (medium format).');
     	$tokens['pdfw']['pdf_watermark-large'] = t('The current date (large format).');
     	$tokens['pdfw']['pdf_watermark-yy'] = t('The current year (2 digits).');
     	$tokens['pdfw']['pdf_watermark-yyyy'] = t('The current year (4 digits).');
     	$tokens['pdfw']['pdf_watermark-month'] = t('The current month (full word).');
     	$tokens['pdfw']['pdf_watermark-mo'] = t('The current month (abbreviated word).');
     	$tokens['pdfw']['pdf_watermark-mn'] = t('The current month (two digit, zero padded).');
     	$tokens['pdfw']['pdf_watermark-m'] = t('The current month (one or two digit).');
     	$tokens['pdfw']['pdf_watermark-day'] = t('The current day (full word)');
     	$tokens['pdfw']['pdf_watermark-ddd'] = t('The current day (abbreviation)');
     	$tokens['pdfw']['pdf_watermark-dd'] = t('The current day (two digit, zero-padded)');
     	$tokens['pdfw']['pdf_watermark-d'] = t('The current day (one or two digit)');
     	$tokens['pdfw']['pdf_watermark-hh'] = t('The current hour in 12-hour format(two digit, zero-padded)');
     	$tokens['pdfw']['pdf_watermark-ii'] = t('The current minute (two digit, zero-padded)');
     	$tokens['pdfw']['pdf_watermark-ss'] = t('The current second (two digit, zero-padded)');
     }
     return $tokens;
}
 
/**
 * Implements hook_token_values().
 */

function _pdf_watermark_token_values($type, $object = NULL, $options = array()) {
  global $user;
  profile_load_profile($user);
  $values = array();
  
  if ($type == 'pdfw') {
    	$values['pdf_watermark-user'] = $user->name;
    	$values['pdf_watermark-email'] = $user->mail;
    	if (module_exists('profile')){
    	$values['pdf_watermark-fullname'] = $user->profile_first_name . " " . $user->profile_last_name;
    	}
    	$values['pdf_watermark-small'] = format_date($date, 'small');
    	$values['pdf_watermark-medium'] = format_date($date, 'medium');
    	$values['pdf_watermark-large'] = format_date($date, 'large');
    	$values['pdf_watermark-yy'] = date('y');
    	$values['pdf_watermark-yyyy'] = date('Y');
    	$values['pdf_watermark-month'] = date('F');
    	$values['pdf_watermark-mo'] = date('M');
    	$values['pdf_watermark-mn'] = date('m');
    	$values['pdf_watermark-m'] = date('n');
    	$values['pdf_watermark-day'] = date('l');
    	$values['pdf_watermark-ddd'] = date('D');
    	$values['pdf_watermark-dd'] = date('d');
    	$values['pdf_watermark-d'] = date('j');
    	$values['pdf_watermark-hh'] = date('h');
    	$values['pdf_watermark-ii'] = date('i');
    	$values['pdf_watermark-ss'] = date('s');
  }
  return $values;
}

