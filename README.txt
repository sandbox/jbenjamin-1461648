PDF Watermark

	- Updated for Drupal 7 & Ubercart 3
	- Rolled tokens into the .module file 
	- Added additional token support. 

=======================
Install
=======================

#### Requirements

	FPDF: (http://fpdf.org/)
	
	Download the latest version, unpack to your sites/all/libraries directory.  
	IF this directory doesn't exist, you'll have to create it.

	Your path should be sites/all/libraries/fdpf
		
	FPDI: (http://www.setasign.de/products/pdf-php-solutions/fpdi/downloads/)
	
	Dowwnload the latest version, unpack to your sites/all/libraries directory.

	Your path should be sites/all/libraries/fpdi

	You will also need to download the fpdf_tpl archive from the site above 
	and place the fpdf_tpl.php file within the fpdi directory.

#### Dependencies

	PDF Watermark requires the Token module.

